package ru.solovgeo.tochkatestapp

import com.nhaarman.mockito_kotlin.eq
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.Mock
import org.mockito.Mockito
import org.mockito.Mockito.verify
import org.mockito.junit.MockitoJUnitRunner
import ru.solovgeo.tochkatestapp.data.common.IImageLoader
import ru.solovgeo.tochkatestapp.data.common.socialnetworkdataprovider.ISocialNetworkDataProvider
import ru.solovgeo.tochkatestapp.domain.SearchGithubUsers
import ru.solovgeo.tochkatestapp.presentation.mainscreen.IMainScreenRouter
import ru.solovgeo.tochkatestapp.presentation.mainscreen.MainScreenViewModel

@RunWith(MockitoJUnitRunner::class)
class MainScreenViewModelTest {

    companion object {
        val USER_NAME = "Lorem Ipsum"
        val SEARCH_TEXT = "Test"
    }

    lateinit var mViewModel: MainScreenViewModel
    @Mock
    lateinit var mSocialNetworkDataProvider: ISocialNetworkDataProvider
    @Mock
    lateinit var mImageLoader: IImageLoader
    @Mock
    lateinit var mMainScreenRouter: IMainScreenRouter
    @Mock
    lateinit var mSearchGithubUsers: SearchGithubUsers

    @Before
    fun setUp() {
        mViewModel = MainScreenViewModel(mSocialNetworkDataProvider, mImageLoader,
                mMainScreenRouter, mSearchGithubUsers)
    }

    @Test
    fun UserName() {
        mViewModel.userName(USER_NAME)
        assert(mViewModel.userName.get() == USER_NAME)
    }

    @Test
    fun logout() {
        mViewModel.logout()
        verify<IMainScreenRouter>(mMainScreenRouter).startLoginActivity()
        verify<ISocialNetworkDataProvider>(mSocialNetworkDataProvider).logout()
    }

    @Test
    fun getUserList() {
        mViewModel.getFirstSearchPage(SEARCH_TEXT)
        assert(mViewModel.showProgress.get())
        verify<SearchGithubUsers>(mSearchGithubUsers).execute(any(), eq(SearchGithubUsers.Params(SEARCH_TEXT, 0)))

        mViewModel.getNextSearchPage(1)
        assert(!mViewModel.showProgress.get())
        verify<SearchGithubUsers>(mSearchGithubUsers).execute(any(), eq(SearchGithubUsers.Params(SEARCH_TEXT, 1)))
    }

    private fun <T> any(): T {
        Mockito.any<T>()
        return uninitialized()
    }

    private fun <T> uninitialized(): T = null as T

}