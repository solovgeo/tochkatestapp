package ru.solovgeo.tochkatestapp

import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.Mock
import org.mockito.Mockito.*
import org.mockito.junit.MockitoJUnitRunner
import ru.solovgeo.tochkatestapp.data.common.socialnetworkmanagers.IFacebookManager
import ru.solovgeo.tochkatestapp.data.common.socialnetworkmanagers.IGoogleManager
import ru.solovgeo.tochkatestapp.data.common.socialnetworkmanagers.IVKManager
import ru.solovgeo.tochkatestapp.presentation.login.ILoginRouter
import ru.solovgeo.tochkatestapp.presentation.login.LoginViewModel
import ru.solovgeo.tochkatestapp.presentation.mainscreen.MainScreenActivity


@RunWith(MockitoJUnitRunner::class)
class LoginViewModelTest {

    @Mock
    private lateinit var facebookManager: IFacebookManager
    @Mock
    private lateinit var googleManager: IGoogleManager
    @Mock
    private lateinit var vkManager: IVKManager
    @Mock
    private lateinit var loginRouter: ILoginRouter

    private lateinit var mViewModel: LoginViewModel
    @Before
    fun setUp() {

        mViewModel = LoginViewModel(facebookManager, googleManager, vkManager, loginRouter)

    }

    @Test
    fun hasFacebookToken() {
        setManagersTokenResponce(true, false, false)
        mViewModel.tryToLoginByExistingToken()
        verify<ILoginRouter>(loginRouter).startMainActivity(MainScreenActivity.DataProviderType.FACEBOOK)
    }

    @Test
    fun hasGoogleToken() {
        setManagersTokenResponce(false, true, false)
        mViewModel.tryToLoginByExistingToken()
        verify<ILoginRouter>(loginRouter).startMainActivity(MainScreenActivity.DataProviderType.GOOGLE)
    }

    @Test
    fun hasVkToken() {
        setManagersTokenResponce(false, false, true)
        mViewModel.tryToLoginByExistingToken()
        verify<ILoginRouter>(loginRouter).startMainActivity(MainScreenActivity.DataProviderType.VK)
    }

    @Test
    fun noTokens() {
        setManagersTokenResponce(false, false, false)
        mViewModel.tryToLoginByExistingToken()
        verifyZeroInteractions(loginRouter)
    }

    private fun setManagersTokenResponce(facebook: Boolean, google: Boolean, vk: Boolean) {
        `when`(facebookManager.isUserLoggedIn()).thenReturn(facebook)
        `when`(googleManager.isUserLoggedIn()).thenReturn(google)
        `when`(vkManager.isUserLoggedIn()).thenReturn(vk)
    }


}