package ru.solovgeo.tochkatestapp

import com.squareup.okhttp.mockwebserver.Dispatcher
import com.squareup.okhttp.mockwebserver.MockResponse
import com.squareup.okhttp.mockwebserver.MockWebServer
import com.squareup.okhttp.mockwebserver.RecordedRequest
import io.reactivex.observers.TestObserver
import org.junit.Assert.assertEquals
import org.junit.Before
import org.junit.Test
import ru.solovgeo.tochkatestapp.data.entity.GithubUserList
import ru.solovgeo.tochkatestapp.data.net.ApiModule
import ru.solovgeo.tochkatestapp.data.net.GithubApi
import ru.solovgeo.tochkatestapp.other.Const
import ru.solovgeo.tochkatestapp.utils.TestUtils


class GithubApiTest {

    lateinit var mServer: MockWebServer
    lateinit var mApiInterface: GithubApi
    lateinit var mTestUtils: TestUtils

    companion object {
        val SEARCH_TEXT = "test"
        val PAGE_NUM = 3
    }

    @Before
    @Throws(Exception::class)
    fun setup() {
        mServer = MockWebServer()
        mTestUtils = TestUtils()
        mServer.start()
        val dispatcher = object : Dispatcher() {
            @Throws(InterruptedException::class)
            override fun dispatch(request: RecordedRequest): MockResponse {
                if (request.path == "/search/users" + "?q=" + SEARCH_TEXT + "&page=" + PAGE_NUM + "&page_size=" + Const.PAGINATION_PAGE_SIZE) {
                    return MockResponse().setResponseCode(200)
                            .setBody(mTestUtils.readString("json/GithubUserList.json"))
                }
                return MockResponse().setResponseCode(404)
            }
        }

        mServer.setDispatcher(dispatcher)
        val baseUrl = mServer.url("/")
        mApiInterface = ApiModule.getApiInterface(baseUrl.toString())
    }

    @Test
    @Throws(Exception::class)
    fun testGetRepositories() {

        val testObserver = TestObserver<GithubUserList>()
        mApiInterface.getUsers(SEARCH_TEXT, PAGE_NUM, Const.PAGINATION_PAGE_SIZE).subscribe(testObserver)

        testObserver.assertNoErrors()
        testObserver.assertValueCount(1)

        val actual: GithubUserList = testObserver.values().get(0)

        assertEquals(28982, actual.total_count)
        assertEquals(30, actual.items.size)
        assertEquals(11500783, actual.items[0].id)
    }

}