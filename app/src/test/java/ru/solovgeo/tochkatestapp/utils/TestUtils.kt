package ru.solovgeo.tochkatestapp.utils

import java.io.IOException
import java.nio.charset.Charset


class TestUtils {

    fun readString(fileName: String): String? {
        val stream = javaClass.classLoader.getResourceAsStream(fileName)
        try {
            val size = stream!!.available()
            val buffer = ByteArray(size)
            stream.read(buffer)
            return String(buffer, Charset.forName("UTF-8"))
        } catch (e: IOException) {
            e.printStackTrace()
            return null
        } finally {
            try {
                stream?.close()
            } catch (e: IOException) {
                e.printStackTrace()
            }

        }

    }

}
