package ru.solovgeo.tochkatestapp.domain

import io.reactivex.Observable
import ru.solovgeo.tochkatestapp.data.entity.GithubUser
import ru.solovgeo.tochkatestapp.data.repository.IGithubDataRepo
import ru.solovgeo.tochkatestapp.domain.executor.PostExecutionThread
import ru.solovgeo.tochkatestapp.domain.executor.ThreadExecutor
import javax.inject.Inject

class SearchGithubUsers @Inject constructor(threadExecutor: ThreadExecutor,
                                            postExecutionThread: PostExecutionThread,
                                            githubDataRepo: IGithubDataRepo)
    : UseCase<List<GithubUser>, SearchGithubUsers.Params>(threadExecutor, postExecutionThread) {

    private val mGithubDataRepo = githubDataRepo
    override fun buildUseCaseObservable(params: Params): Observable<List<GithubUser>> {
        return mGithubDataRepo.searchGithubUsers(params.searchText, params.currentPage)
    }

    data class Params(val searchText: String, val currentPage: Int)
}
