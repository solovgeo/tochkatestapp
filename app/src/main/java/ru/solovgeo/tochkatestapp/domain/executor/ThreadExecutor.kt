package ru.solovgeo.tochkatestapp.domain.executor

import java.util.concurrent.Executor

public interface ThreadExecutor : Executor
