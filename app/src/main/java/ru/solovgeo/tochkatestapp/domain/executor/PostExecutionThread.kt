package ru.solovgeo.tochkatestapp.domain.executor

interface PostExecutionThread {
    val scheduler: io.reactivex.Scheduler
}
