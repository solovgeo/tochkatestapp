package ru.solovgeo.tochkatestapp.other

class Const {
    companion object {
        val BASE_URL: String = "https://api.github.com/"
        val PAGINATION_PAGE_SIZE = 30
    }
}