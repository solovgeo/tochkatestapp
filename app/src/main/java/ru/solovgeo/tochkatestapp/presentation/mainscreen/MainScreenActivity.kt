package ru.solovgeo.tochkatestapp.presentation.mainscreen

import android.content.Context
import android.content.Intent
import android.databinding.DataBindingComponent
import android.databinding.DataBindingUtil
import android.os.Bundle
import android.support.design.widget.NavigationView
import android.support.v4.widget.DrawerLayout
import android.support.v7.app.ActionBarDrawerToggle
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.support.v7.widget.Toolbar
import android.util.Log
import android.widget.EditText
import com.jakewharton.rxbinding2.widget.RxTextView
import io.reactivex.Observer
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import ru.solovgeo.tochkatestapp.R
import ru.solovgeo.tochkatestapp.databinding.ActivityMainBinding
import ru.solovgeo.tochkatestapp.databinding.ActivityMainDrawerHeaderBinding
import ru.solovgeo.tochkatestapp.presentation.AndroidApplication
import ru.solovgeo.tochkatestapp.presentation.utils.EndlessRecyclerViewScrollListener
import ru.solovgeo.tochkatestapp.presentation.utils.injection.components.DaggerMainScreenComponent
import ru.solovgeo.tochkatestapp.presentation.utils.injection.modules.MainScreenModule
import java.util.concurrent.TimeUnit
import javax.inject.Inject


class MainScreenActivity : AppCompatActivity() {

    @Inject
    lateinit var mViewModel: MainScreenViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        val dataProviderType = intent.getSerializableExtra(DATA_PROVIDER_TYPE) as DataProviderType
        inject(dataProviderType)
        initDataBinding()
        initDrawer(initToolbar())
        initSearch()
        initRecyclerView()
    }

    override fun onDestroy() {
        mViewModel.onDestroy()
        super.onDestroy()
    }

    private fun inject(dataProviderType: DataProviderType) {
        DaggerMainScreenComponent.builder()
                .applicationComponent((application as AndroidApplication).applicationComponent)
                .mainScreenModule(MainScreenModule(dataProviderType, this))
                .build()
                .inject(this)
    }

    private fun initDataBinding() {
        val binding: ActivityMainBinding = DataBindingUtil.setContentView(this, R.layout.activity_main)
        binding.vm = mViewModel

        DataBindingUtil.setDefaultComponent(object : DataBindingComponent {
            override fun getGithubUserViewModel(): GithubUserViewModel {
                return GithubUserViewModel()
            }

            override fun getMainScreenViewModel(): MainScreenViewModel {
                return mViewModel
            }
        })
        val headerBinding: ActivityMainDrawerHeaderBinding = DataBindingUtil.inflate(layoutInflater,
                R.layout.activity_main_drawer_header, binding.leftDrawer, false)
        headerBinding.vm = mViewModel
        binding.leftDrawer.addHeaderView(headerBinding.root)
        binding.leftDrawer.setNavigationItemSelectedListener(NavigationView.OnNavigationItemSelectedListener { item ->
            if (item.itemId == R.id.nav_logout) {
                mViewModel.logout()
                return@OnNavigationItemSelectedListener true
            }
            false
        })
    }

    private fun initSearch() {
        val searchBox: EditText = findViewById(R.id.search_box)
        RxTextView.textChanges(searchBox)
                .debounce(600, TimeUnit.MILLISECONDS)
                .filter({ charSequence: CharSequence -> charSequence.length >= 2 })
                .map({ charSequence: CharSequence -> charSequence.toString() })
                .distinctUntilChanged()
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(object : Observer<String> {
                    override fun onSubscribe(d: Disposable) {}
                    override fun onComplete() {
                        Log.d("RxSearch", "onComplete")
                    }

                    override fun onError(e: Throwable) {
                        Log.e("RxSearch", "onError " + e.message)
                    }

                    override fun onNext(text: String) {
                        mViewModel.getFirstSearchPage(text)
                    }
                })
    }

    private fun initRecyclerView() {
        val recyclerView: RecyclerView = findViewById(R.id.recycler_view)
        recyclerView.adapter = mViewModel.adapter
        val layoutManager = LinearLayoutManager(this)
        val scrollListener = object : EndlessRecyclerViewScrollListener(layoutManager) {
            override fun onLoadMore(page: Int) {
                mViewModel.getNextSearchPage(page)
            }
        }
        recyclerView.layoutManager = layoutManager
        recyclerView.addOnScrollListener(scrollListener)
    }

    private fun initDrawer(toolbar: Toolbar) {
        val mDrawer: DrawerLayout = findViewById(R.id.drawer_layout)
        val drawerToggle = ActionBarDrawerToggle(this, mDrawer, toolbar, R.string.drawer_open, R.string.drawer_close)
        drawerToggle.syncState()
        mDrawer.addDrawerListener(drawerToggle)

    }

    private fun initToolbar(): Toolbar {
        val toolbar: Toolbar = findViewById(R.id.toolbar)
        setSupportActionBar(toolbar)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.setHomeButtonEnabled(true)
        return toolbar
    }

    companion object {
        val DATA_PROVIDER_TYPE = "DATA_PROVIDER_TYPE"
        fun callingIntent(context: Context, dataProviderType: DataProviderType): Intent {
            val intent = Intent(context, MainScreenActivity::class.java)
            intent.putExtra(DATA_PROVIDER_TYPE, dataProviderType)
            return intent
        }
    }

    enum class DataProviderType {
        FACEBOOK,
        GOOGLE,
        VK
    }
}