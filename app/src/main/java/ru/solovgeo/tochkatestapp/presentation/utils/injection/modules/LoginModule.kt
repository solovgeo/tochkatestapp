package ru.solovgeo.tochkatestapp.presentation.utils.injection.modules

import android.support.v7.app.AppCompatActivity
import dagger.Module
import dagger.Provides
import ru.solovgeo.tochkatestapp.data.common.socialnetworkmanagers.*
import ru.solovgeo.tochkatestapp.presentation.login.ILoginRouter
import ru.solovgeo.tochkatestapp.presentation.login.LoginRouter
import ru.solovgeo.tochkatestapp.presentation.utils.injection.scopes.LoginScope

@Module
class LoginModule(appCompatActivity: AppCompatActivity) {
    private val mAppCompatActivity = appCompatActivity

    @Provides
    @LoginScope
    fun provideActivity(): AppCompatActivity = mAppCompatActivity

    @Provides
    @LoginScope
    fun provideRouter(loginRouter: LoginRouter): ILoginRouter = loginRouter

    @Provides
    @LoginScope
    fun provideGoogleManager(googleManager: GoogleManager): IGoogleManager = googleManager

    @Provides
    @LoginScope
    fun provideFacebookManager(facebookManager: FacebookManager): IFacebookManager = facebookManager

    @Provides
    @LoginScope
    fun provideVKManager(vkManager: VKManager): IVKManager = vkManager
}