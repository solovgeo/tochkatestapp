package ru.solovgeo.tochkatestapp.presentation.login

import ru.solovgeo.tochkatestapp.presentation.mainscreen.MainScreenActivity

interface ILoginRouter {
    fun startMainActivity(dataProviderType: MainScreenActivity.DataProviderType)
}