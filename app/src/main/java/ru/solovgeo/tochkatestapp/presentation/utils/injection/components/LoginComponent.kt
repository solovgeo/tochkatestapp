package ru.solovgeo.tochkatestapp.presentation.utils.injection.components

import dagger.Component
import ru.solovgeo.tochkatestapp.presentation.login.LoginActivity
import ru.solovgeo.tochkatestapp.presentation.utils.injection.modules.LoginModule
import ru.solovgeo.tochkatestapp.presentation.utils.injection.scopes.LoginScope


@LoginScope
@Component(dependencies = arrayOf(ApplicationComponent::class), modules = arrayOf(LoginModule::class))
interface LoginComponent {
    fun inject(loginActivity: LoginActivity) {}
}