package ru.solovgeo.tochkatestapp.presentation.mainscreen

import android.databinding.BindingAdapter
import android.databinding.ObservableBoolean
import android.databinding.ObservableField
import android.net.Uri
import android.util.Log
import de.hdodenhof.circleimageview.CircleImageView
import io.reactivex.observers.DisposableObserver
import ru.solovgeo.tochkatestapp.data.common.IImageLoader
import ru.solovgeo.tochkatestapp.data.common.socialnetworkdataprovider.ISocialNetworkDataProvider
import ru.solovgeo.tochkatestapp.data.entity.GithubUser
import ru.solovgeo.tochkatestapp.domain.SearchGithubUsers
import ru.solovgeo.tochkatestapp.presentation.utils.injection.scopes.MainScreenScope
import javax.inject.Inject


@MainScreenScope
class MainScreenViewModel @Inject constructor(private val mSocialNetworkDataProvider: ISocialNetworkDataProvider,
                                              private val mImageLoader: IImageLoader,
                                              private val mMainScreenRouter: IMainScreenRouter,
                                              private val mSearchGithubUsers: SearchGithubUsers) : ISocialNetworkDataProvider.AsyncCallsHandler {

    val userName: ObservableField<String> = ObservableField()
    val userPhoto: ObservableField<Uri> = ObservableField()
    val showProgress: ObservableBoolean = ObservableBoolean(false)
    val adapter: GithubUsersAdapter = GithubUsersAdapter()
    private var searchString: String? = null

    init {
        userName.set(mSocialNetworkDataProvider.userName(this))
        userPhoto.set(mSocialNetworkDataProvider.userPhoto(this))
    }

    fun logout() {
        mSocialNetworkDataProvider.logout()
        mMainScreenRouter.startLoginActivity()
    }

    override fun userName(userName: String?) {
        this.userName.set(userName)
    }

    override fun userPhoto(photo: Uri?) {
        this.userPhoto.set(photo)
    }

    fun getFirstSearchPage(text: String) {
        showProgress.set(true)
        searchString = text
        adapter.clearData()
        mSearchGithubUsers.execute(CustomObserver(), SearchGithubUsers.Params(text, 0))
    }

    fun getNextSearchPage(page: Int) {
        if (searchString != null) {
            mSearchGithubUsers.execute(CustomObserver(), SearchGithubUsers.Params(searchString!!, page))
        }
    }

    inner class CustomObserver : DisposableObserver<List<GithubUser>>() {
        override fun onError(e: Throwable) {
            Log.e("DisposableObserver", e.toString())
            e.printStackTrace()
        }

        override fun onNext(githubUsers: List<GithubUser>) {
            val userViewModels: List<GithubUserViewModel> = githubUsers.map { mapUsersOnViewModel(it) }
            showProgress.set(false)
            adapter.addData(userViewModels)
        }

        override fun onComplete() {}
    }

    private fun mapUsersOnViewModel(user: GithubUser): GithubUserViewModel {
        val githubUserViewModel = GithubUserViewModel()
        githubUserViewModel.userName.set(user.login)
        githubUserViewModel.userImage.set(mImageLoader.getImageRequestCreator(Uri.parse(user.avatar_url)))
        return githubUserViewModel
    }

    fun onDestroy() {
        mSearchGithubUsers.dispose()
    }

    @BindingAdapter("android:src")
    fun setImageFromUri(view: CircleImageView, uri: Uri?) {
        if (uri != null) {
            mImageLoader.getImageRequestCreator(uri, view)
        }
    }
}


