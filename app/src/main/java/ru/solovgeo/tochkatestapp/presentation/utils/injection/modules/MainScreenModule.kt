package ru.solovgeo.tochkatestapp.presentation.utils.injection.modules

import android.support.v7.app.AppCompatActivity
import dagger.Module
import dagger.Provides
import ru.solovgeo.tochkatestapp.data.common.socialnetworkdataprovider.FacebookDataProvider
import ru.solovgeo.tochkatestapp.data.common.socialnetworkdataprovider.GoogleDataProvider
import ru.solovgeo.tochkatestapp.data.common.socialnetworkdataprovider.ISocialNetworkDataProvider
import ru.solovgeo.tochkatestapp.data.common.socialnetworkdataprovider.VKDataProvider
import ru.solovgeo.tochkatestapp.presentation.mainscreen.IMainScreenRouter
import ru.solovgeo.tochkatestapp.presentation.mainscreen.MainScreenActivity
import ru.solovgeo.tochkatestapp.presentation.mainscreen.MainScreenRouter
import ru.solovgeo.tochkatestapp.presentation.utils.injection.scopes.MainScreenScope

@Module
class MainScreenModule(dataProviderType: MainScreenActivity.DataProviderType, appCompatActivity: AppCompatActivity) {
    private val mDataProviderType = dataProviderType
    private val mAppCompatActivity = appCompatActivity

    @Provides
    @MainScreenScope
    fun provideAppCompatActivity(): AppCompatActivity = mAppCompatActivity

    @Provides
    @MainScreenScope
    fun provideSocialNetworkDataProvider(): ISocialNetworkDataProvider {
        when (mDataProviderType) {
            MainScreenActivity.DataProviderType.FACEBOOK -> return FacebookDataProvider(mAppCompatActivity)
            MainScreenActivity.DataProviderType.GOOGLE -> return GoogleDataProvider(mAppCompatActivity)
            MainScreenActivity.DataProviderType.VK -> return VKDataProvider()
        }
    }

    @Provides
    @MainScreenScope
    fun provideRouter(mainScreenRouter: MainScreenRouter): IMainScreenRouter = mainScreenRouter

}