package ru.solovgeo.tochkatestapp.presentation.mainscreen

interface IMainScreenRouter {
    fun startLoginActivity()
}