package ru.solovgeo.tochkatestapp.presentation.utils.injection.components

import android.content.Context
import dagger.Component
import ru.solovgeo.tochkatestapp.data.common.IImageLoader
import ru.solovgeo.tochkatestapp.data.net.GithubApi
import ru.solovgeo.tochkatestapp.data.repository.IGithubDataRepo
import ru.solovgeo.tochkatestapp.domain.executor.PostExecutionThread
import ru.solovgeo.tochkatestapp.domain.executor.ThreadExecutor
import ru.solovgeo.tochkatestapp.presentation.AndroidApplication
import ru.solovgeo.tochkatestapp.presentation.utils.injection.modules.ApplicationModule
import javax.inject.Singleton

@Singleton
@Component(modules = arrayOf(ApplicationModule::class))
interface ApplicationComponent {
    fun inject(androidApplication: AndroidApplication)

    fun context(): Context
    fun imageLoader(): IImageLoader
    fun threadExecutor(): ThreadExecutor
    fun postThreadExecutor(): PostExecutionThread
    fun gitHubDataRepo(): IGithubDataRepo
    fun githubApi(): GithubApi
}