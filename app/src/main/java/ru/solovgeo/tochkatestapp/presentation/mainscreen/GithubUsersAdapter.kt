package ru.solovgeo.tochkatestapp.presentation.mainscreen

import android.databinding.DataBindingUtil
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.ViewGroup
import ru.solovgeo.tochkatestapp.R
import ru.solovgeo.tochkatestapp.databinding.LayoutGithubUserRowBinding

class GithubUsersAdapter() : RecyclerView.Adapter<GithubUsersAdapter.GithubUserViewHolder>() {
    private val notifications: MutableList<GithubUserViewModel> = ArrayList()

    override fun onBindViewHolder(holder: GithubUserViewHolder?, position: Int) {
        val item = notifications[position]
        holder?.bind(item)
    }

    fun clearData() {
        if (itemCount > 0) {
            val previousCount = itemCount
            notifications.clear()
            notifyItemRangeRemoved(0, previousCount)
        }
    }

    fun addData(data: List<GithubUserViewModel>) {
        val previousCount = itemCount
        this.notifications.addAll(data)
        notifyItemRangeInserted(previousCount, data.size)
    }

    override fun onCreateViewHolder(parent: ViewGroup?, viewType: Int): GithubUserViewHolder {
        val layoutInflater = LayoutInflater.from(parent?.getContext())
        val binding: LayoutGithubUserRowBinding = DataBindingUtil.inflate(
                layoutInflater, R.layout.layout_github_user_row, parent, false)
        return GithubUserViewHolder(binding)
    }

    override fun getItemCount(): Int = notifications.size

    class GithubUserViewHolder(binding: LayoutGithubUserRowBinding) : RecyclerView.ViewHolder(binding.root) {
        private val mBinding = binding

        fun bind(viewModel: GithubUserViewModel) {
            mBinding.vm = viewModel
            mBinding.executePendingBindings()
        }
    }
}