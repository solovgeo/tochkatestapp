package ru.solovgeo.tochkatestapp.presentation

import android.app.Application
import com.vk.sdk.VKSdk
import ru.solovgeo.tochkatestapp.presentation.utils.injection.components.ApplicationComponent
import ru.solovgeo.tochkatestapp.presentation.utils.injection.components.DaggerApplicationComponent
import ru.solovgeo.tochkatestapp.presentation.utils.injection.modules.ApplicationModule

class AndroidApplication : Application() {

    val applicationComponent: ApplicationComponent = DaggerApplicationComponent.builder()
            .applicationModule(ApplicationModule(this))
            .build()

    override fun onCreate() {
        super.onCreate()
        applicationComponent.inject(this)
        initVK()
    }

    private fun initVK() = VKSdk.initialize(this)
}