package ru.solovgeo.tochkatestapp.presentation.login

import android.content.Intent
import com.facebook.FacebookCallback
import com.facebook.FacebookException
import com.facebook.login.LoginResult
import com.google.android.gms.auth.api.signin.GoogleSignInResult
import com.vk.sdk.VKAccessToken
import com.vk.sdk.VKCallback
import com.vk.sdk.api.VKError
import ru.solovgeo.tochkatestapp.data.common.socialnetworkmanagers.GoogleManager
import ru.solovgeo.tochkatestapp.data.common.socialnetworkmanagers.IFacebookManager
import ru.solovgeo.tochkatestapp.data.common.socialnetworkmanagers.IGoogleManager
import ru.solovgeo.tochkatestapp.data.common.socialnetworkmanagers.IVKManager
import ru.solovgeo.tochkatestapp.presentation.mainscreen.MainScreenActivity
import ru.solovgeo.tochkatestapp.presentation.utils.injection.scopes.LoginScope
import javax.inject.Inject

@LoginScope
class LoginViewModel @Inject constructor(facebookManager: IFacebookManager,
                                         googleManager: IGoogleManager,
                                         vkManager: IVKManager,
                                         loginRouter: ILoginRouter) {

    private val mFacebookManager = facebookManager
    private val mGoogleManager = googleManager
    private val mVKManager = vkManager
    private val mLoginRouter = loginRouter

    fun loginByFacebook() {
        mFacebookManager.login(object : FacebookCallback<LoginResult> {
            override fun onError(error: FacebookException?) {}
            override fun onCancel() {}
            override fun onSuccess(result: LoginResult?) = startMainActivity(MainScreenActivity.DataProviderType.FACEBOOK)
        })
    }

    fun loginByGoogle() {
        mGoogleManager.login()
    }

    fun loginByVK() {
        mVKManager.login()
    }

    fun facebookOnActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        mFacebookManager.onActivityResult(requestCode, resultCode, data)
    }

    fun googleOnActivityResult(intent: Intent?) {
        mGoogleManager.onActivityResult(intent, object : GoogleManager.GoogleCallback {
            override fun onError() {}
            override fun onSuccess(result: GoogleSignInResult) = startMainActivity(MainScreenActivity.DataProviderType.GOOGLE)

        })
    }

    fun vkOnActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        mVKManager.onActivityResult(requestCode, resultCode, data, object : VKCallback<VKAccessToken> {

            override fun onError(error: VKError?) {}
            override fun onResult(res: VKAccessToken?) = startMainActivity(MainScreenActivity.DataProviderType.VK)
        })
    }

    private fun startMainActivity(dataProviderType: MainScreenActivity.DataProviderType) {
        mLoginRouter.startMainActivity(dataProviderType)
    }

    fun tryToLoginByExistingToken() {

        if (mFacebookManager.isUserLoggedIn()) {
            startMainActivity(MainScreenActivity.DataProviderType.FACEBOOK)
        }

        if (mVKManager.isUserLoggedIn()) {
            startMainActivity(MainScreenActivity.DataProviderType.VK)
        }

        if (mGoogleManager.isUserLoggedIn()) {
            startMainActivity(MainScreenActivity.DataProviderType.GOOGLE)
        }
    }
}