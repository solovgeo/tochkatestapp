package ru.solovgeo.tochkatestapp.presentation.mainscreen

import android.databinding.BindingAdapter
import android.databinding.ObservableField
import android.widget.ImageView
import com.squareup.picasso.RequestCreator

class GithubUserViewModel {
    val userImage: ObservableField<RequestCreator> = ObservableField()
    val userName: ObservableField<String> = ObservableField()

    @BindingAdapter("android:src")
    fun setLeftMargin(view: ImageView, requestCreator: RequestCreator?) {
        requestCreator?.into(view)
    }
}