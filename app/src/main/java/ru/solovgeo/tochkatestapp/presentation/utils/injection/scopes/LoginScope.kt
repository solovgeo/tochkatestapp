package ru.solovgeo.tochkatestapp.presentation.utils.injection.scopes

import javax.inject.Scope

@Scope
@Retention(AnnotationRetention.RUNTIME)
annotation class LoginScope