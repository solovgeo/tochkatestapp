package ru.solovgeo.tochkatestapp.presentation.login

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.databinding.DataBindingUtil
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import ru.solovgeo.tochkatestapp.R
import ru.solovgeo.tochkatestapp.data.common.socialnetworkmanagers.RequestCode
import ru.solovgeo.tochkatestapp.databinding.ActivityLoginBinding
import ru.solovgeo.tochkatestapp.presentation.AndroidApplication
import ru.solovgeo.tochkatestapp.presentation.utils.injection.components.DaggerLoginComponent
import ru.solovgeo.tochkatestapp.presentation.utils.injection.modules.LoginModule
import javax.inject.Inject


class LoginActivity : AppCompatActivity() {

    @Inject
    lateinit var mViewModel: LoginViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        inject()
        val binding: ActivityLoginBinding = DataBindingUtil.setContentView(this, R.layout.activity_login)
        binding.vm = mViewModel
    }

    override fun onStart() {
        super.onStart()
        mViewModel.tryToLoginByExistingToken()
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (resultCode == Activity.RESULT_OK) {
            when (requestCode) {
                RequestCode.FACEBOOK ->
                    mViewModel.facebookOnActivityResult(requestCode, resultCode, data)
                RequestCode.GOOGLE ->
                    mViewModel.googleOnActivityResult(data)
                RequestCode.VK ->
                    mViewModel.vkOnActivityResult(requestCode, resultCode, data)
            }
        }
    }

    private fun inject() {
        DaggerLoginComponent.builder()
                .applicationComponent((application as AndroidApplication).applicationComponent)
                .loginModule(LoginModule(this))
                .build()
                .inject(this)
    }

    companion object {
        fun callingIntent(context: Context): Intent {
            return Intent(context, LoginActivity::class.java)
        }
    }

}
