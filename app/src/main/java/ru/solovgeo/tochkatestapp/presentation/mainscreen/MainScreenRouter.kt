package ru.solovgeo.tochkatestapp.presentation.mainscreen

import android.support.v7.app.AppCompatActivity
import ru.solovgeo.tochkatestapp.presentation.login.LoginActivity
import javax.inject.Inject

class MainScreenRouter @Inject constructor(mainActivity: AppCompatActivity) : IMainScreenRouter {
    private val mMainActivity = mainActivity
    override fun startLoginActivity() {
        val intent = LoginActivity.callingIntent(mMainActivity)
        mMainActivity.startActivity(intent)
        mMainActivity.finish()
    }
}