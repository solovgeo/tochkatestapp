package ru.solovgeo.tochkatestapp.presentation.login

import android.support.v7.app.AppCompatActivity
import ru.solovgeo.tochkatestapp.presentation.mainscreen.MainScreenActivity
import ru.solovgeo.tochkatestapp.presentation.utils.injection.scopes.LoginScope
import javax.inject.Inject

@LoginScope
class LoginRouter @Inject constructor(appCompatActivity: AppCompatActivity) : ILoginRouter {

    private val mLoginActivity = appCompatActivity
    override fun startMainActivity(dataProviderType: MainScreenActivity.DataProviderType) {
        val intent = MainScreenActivity.callingIntent(mLoginActivity, dataProviderType)
        mLoginActivity.startActivity(intent)
        mLoginActivity.finish()
    }
}