package ru.solovgeo.tochkatestapp.presentation.utils.injection.components

import dagger.Component
import ru.solovgeo.tochkatestapp.presentation.mainscreen.MainScreenActivity
import ru.solovgeo.tochkatestapp.presentation.utils.injection.modules.MainScreenModule
import ru.solovgeo.tochkatestapp.presentation.utils.injection.scopes.MainScreenScope

@MainScreenScope
@Component(dependencies = arrayOf(ApplicationComponent::class), modules = arrayOf(MainScreenModule::class))
interface MainScreenComponent {
    fun inject(mainScreenActivity: MainScreenActivity)
}