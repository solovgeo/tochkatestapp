package ru.solovgeo.tochkatestapp.presentation.utils.injection.modules

import android.content.Context
import dagger.Module
import dagger.Provides
import ru.solovgeo.tochkatestapp.data.common.IImageLoader
import ru.solovgeo.tochkatestapp.data.common.ImageLoader
import ru.solovgeo.tochkatestapp.data.net.ApiModule
import ru.solovgeo.tochkatestapp.data.net.GithubApi
import ru.solovgeo.tochkatestapp.data.repository.GithubDataRepo
import ru.solovgeo.tochkatestapp.data.repository.IGithubDataRepo
import ru.solovgeo.tochkatestapp.domain.executor.JobExecutor
import ru.solovgeo.tochkatestapp.domain.executor.PostExecutionThread
import ru.solovgeo.tochkatestapp.domain.executor.ThreadExecutor
import ru.solovgeo.tochkatestapp.domain.executor.UIThread
import ru.solovgeo.tochkatestapp.other.Const
import javax.inject.Singleton

@Module
class ApplicationModule(context: Context) {
    private val mContext = context

    @Provides
    @Singleton
    fun provideContext(): Context = mContext

    @Provides
    @Singleton
    fun provideImageLoader(imageLoader: ImageLoader): IImageLoader = imageLoader

    @Provides
    @Singleton
    fun provideGithubDataRepo(githubDataRepo: GithubDataRepo): IGithubDataRepo = githubDataRepo

    @Provides
    @Singleton
    fun provideGithubApi(): GithubApi = ApiModule.getApiInterface(Const.BASE_URL)

    @Provides
    @Singleton
    internal fun provideThreadExecutor(jobExecutor: JobExecutor): ThreadExecutor = jobExecutor

    @Provides
    @Singleton
    internal fun providePostExecutionThread(uiThread: UIThread): PostExecutionThread = uiThread

}