package ru.solovgeo.tochkatestapp.data.common.socialnetworkmanagers

import android.content.Intent
import android.support.v7.app.AppCompatActivity
import com.vk.sdk.VKAccessToken
import com.vk.sdk.VKCallback
import com.vk.sdk.VKSdk
import ru.solovgeo.tochkatestapp.presentation.utils.injection.scopes.LoginScope
import javax.inject.Inject

@LoginScope
class VKManager @Inject constructor(activity: AppCompatActivity) : IVKManager {

    private val mActivity = activity

    override fun login() {
        VKSdk.login(mActivity)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?, vkCallback: VKCallback<VKAccessToken>) {
        VKSdk.onActivityResult(requestCode, resultCode, data, vkCallback)
    }

    override fun isUserLoggedIn(): Boolean {
        return VKSdk.isLoggedIn()
    }
}
