package ru.solovgeo.tochkatestapp.data.common.socialnetworkmanagers

import android.content.Intent
import android.support.v7.app.AppCompatActivity
import com.google.android.gms.auth.api.Auth
import com.google.android.gms.auth.api.signin.GoogleSignInOptions
import com.google.android.gms.auth.api.signin.GoogleSignInResult
import com.google.android.gms.common.api.GoogleApiClient
import ru.solovgeo.tochkatestapp.presentation.utils.injection.scopes.LoginScope
import javax.inject.Inject

@LoginScope
class GoogleManager @Inject constructor(activity: AppCompatActivity) : IGoogleManager {

    private val mActivity = activity
    private var mGoogleApiClient: GoogleApiClient? = null

    private fun lazyInit() {
        if (mGoogleApiClient == null) {
            mGoogleApiClient = GoogleApiClient.Builder(mActivity)
                    .enableAutoManage(mActivity, null)
                    .addApi(Auth.GOOGLE_SIGN_IN_API, GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN).build())
                    .build()
        }
    }

    override fun login() {
        lazyInit()
        val signInIntent = Auth.GoogleSignInApi.getSignInIntent(mGoogleApiClient)
        mActivity.startActivityForResult(signInIntent, RequestCode.GOOGLE)
    }

    override fun onActivityResult(data: Intent?, googleCallback: GoogleCallback) {
        val result = Auth.GoogleSignInApi.getSignInResultFromIntent(data)
        if (result.isSuccess) {
            googleCallback.onSuccess(result)
        } else {
            googleCallback.onError()
        }
    }

    override fun isUserLoggedIn(): Boolean {
        lazyInit()
        val opr = Auth.GoogleSignInApi.silentSignIn(mGoogleApiClient)
        if (opr.isDone()) {
            return opr.get() != null
        } else {
            return false
        }
    }

    interface GoogleCallback {
        fun onError()
        fun onSuccess(result: GoogleSignInResult)
    }
}


