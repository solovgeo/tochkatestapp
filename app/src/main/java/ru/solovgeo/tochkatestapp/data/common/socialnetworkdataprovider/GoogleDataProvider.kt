package ru.solovgeo.tochkatestapp.data.common.socialnetworkdataprovider

import android.net.Uri
import android.support.v7.app.AppCompatActivity
import com.google.android.gms.auth.api.Auth
import com.google.android.gms.auth.api.signin.GoogleSignInAccount
import com.google.android.gms.auth.api.signin.GoogleSignInOptions
import com.google.android.gms.common.api.GoogleApiClient
import javax.inject.Inject


class GoogleDataProvider @Inject constructor(activity: AppCompatActivity) : ISocialNetworkDataProvider {

    private val mGoogleAccount: GoogleSignInAccount?
    private val mGoogleApiClient: GoogleApiClient = GoogleApiClient.Builder(activity)
            .enableAutoManage(activity, null)
            .addApi(Auth.GOOGLE_SIGN_IN_API, GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN).build())
            .build()

    init {
        val opr = Auth.GoogleSignInApi.silentSignIn(mGoogleApiClient)
        if (opr.isDone) {
            mGoogleAccount = opr.get().signInAccount
        } else {
            mGoogleAccount = null
        }
    }

    override fun userName(asyncCallsHandler: ISocialNetworkDataProvider.AsyncCallsHandler): String? {
        return mGoogleAccount?.displayName
    }

    override fun userPhoto(asyncCallsHandler: ISocialNetworkDataProvider.AsyncCallsHandler): Uri? {
        return mGoogleAccount?.photoUrl
    }

    override fun logout() {
        Auth.GoogleSignInApi.signOut(mGoogleApiClient)
    }
}