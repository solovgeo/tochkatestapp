package ru.solovgeo.tochkatestapp.data.common

import android.net.Uri
import android.widget.ImageView
import com.squareup.picasso.RequestCreator

interface IImageLoader {
    fun getImageRequestCreator(uri: Uri, imageView: ImageView)
    fun getImageRequestCreator(uri: Uri): RequestCreator
}