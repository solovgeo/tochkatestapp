package ru.solovgeo.tochkatestapp.data.common.socialnetworkmanagers

import android.app.Activity
import android.content.Intent
import android.support.v7.app.AppCompatActivity
import com.facebook.AccessToken
import com.facebook.CallbackManager
import com.facebook.FacebookCallback
import com.facebook.login.LoginManager
import com.facebook.login.LoginResult
import ru.solovgeo.tochkatestapp.presentation.utils.injection.scopes.LoginScope
import java.util.*
import javax.inject.Inject

@LoginScope
class FacebookManager @Inject constructor(activity: AppCompatActivity) : IFacebookManager {

    private val mCallbackManager: CallbackManager = CallbackManager.Factory.create()
    private val mActivity: Activity = activity

    override fun login(callback: FacebookCallback<LoginResult>) {
        LoginManager.getInstance().registerCallback(mCallbackManager, callback)
        if (AccessToken.getCurrentAccessToken() != null) {
            LoginManager.getInstance().logOut()
        }
        LoginManager.getInstance().logInWithReadPermissions(mActivity, Arrays.asList("user_status"))
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        mCallbackManager.onActivityResult(requestCode, resultCode, data)
    }

    override fun isUserLoggedIn(): Boolean {
        return AccessToken.getCurrentAccessToken() != null
    }

}