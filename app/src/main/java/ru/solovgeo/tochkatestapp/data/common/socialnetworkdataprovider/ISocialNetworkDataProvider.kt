package ru.solovgeo.tochkatestapp.data.common.socialnetworkdataprovider

import android.net.Uri

interface ISocialNetworkDataProvider {
    fun userName(asyncCallsHandler: ISocialNetworkDataProvider.AsyncCallsHandler): String?
    fun userPhoto(asyncCallsHandler: ISocialNetworkDataProvider.AsyncCallsHandler): Uri?
    fun logout()

    interface AsyncCallsHandler {
        fun userName(userName: String?)
        fun userPhoto(photo: Uri?)
    }
}