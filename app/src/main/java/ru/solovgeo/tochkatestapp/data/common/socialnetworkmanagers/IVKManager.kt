package ru.solovgeo.tochkatestapp.data.common.socialnetworkmanagers

import android.content.Intent
import com.vk.sdk.VKAccessToken
import com.vk.sdk.VKCallback

interface IVKManager {
    fun login()
    fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?, vkCallback: VKCallback<VKAccessToken>)
    fun isUserLoggedIn(): Boolean
}