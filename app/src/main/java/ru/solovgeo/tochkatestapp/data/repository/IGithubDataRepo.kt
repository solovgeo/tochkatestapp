package ru.solovgeo.tochkatestapp.data.repository

import io.reactivex.Observable
import ru.solovgeo.tochkatestapp.data.entity.GithubUser

interface IGithubDataRepo {
    fun searchGithubUsers(text: String, page: Int): Observable<List<GithubUser>>
}