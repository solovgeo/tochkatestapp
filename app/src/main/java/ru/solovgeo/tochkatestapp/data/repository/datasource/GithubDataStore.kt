package ru.solovgeo.tochkatestapp.data.repository.datasource

import io.reactivex.Observable
import ru.solovgeo.tochkatestapp.data.entity.GithubUserList

interface GithubDataStore {

    fun searchGithubUsers(text: String, page: Int, pageSize: Int): Observable<GithubUserList>
}