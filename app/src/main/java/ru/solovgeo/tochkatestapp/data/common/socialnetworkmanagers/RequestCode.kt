package ru.solovgeo.tochkatestapp.data.common.socialnetworkmanagers

import com.facebook.internal.CallbackManagerImpl
import com.vk.sdk.VKServiceActivity


interface RequestCode {
    companion object {
        val FACEBOOK = CallbackManagerImpl.RequestCodeOffset.Login.toRequestCode()
        val GOOGLE = 1001
        val VK = VKServiceActivity.VKServiceType.Authorization.getOuterCode()
    }
}
