package ru.solovgeo.tochkatestapp.data.common.socialnetworkdataprovider

import android.net.Uri
import com.vk.sdk.VKSdk
import com.vk.sdk.api.VKApi
import com.vk.sdk.api.VKApiConst
import com.vk.sdk.api.VKParameters
import com.vk.sdk.api.VKRequest.VKRequestListener
import com.vk.sdk.api.VKResponse
import com.vk.sdk.api.model.VKApiUser
import com.vk.sdk.api.model.VKList

class VKDataProvider : ISocialNetworkDataProvider {

    override fun userName(asyncCallsHandler: ISocialNetworkDataProvider.AsyncCallsHandler): String? {
        val request = VKApi.users().get(VKParameters.from(VKApiConst.FIELDS, "first_name,last_name,photo_200"))
        request.executeWithListener(object : VKRequestListener() {
            override fun onComplete(response: VKResponse?) {
                val user = (response?.parsedModel as VKList<VKApiUser>)[0]
                asyncCallsHandler.userName(user.first_name + " " + user.last_name)
            }
        })
        return null
    }

    override fun userPhoto(asyncCallsHandler: ISocialNetworkDataProvider.AsyncCallsHandler): Uri? {
        val request = VKApi.users().get(VKParameters.from(VKApiConst.FIELDS, "first_name,last_name,photo_200"))
        request.executeWithListener(object : VKRequestListener() {
            override fun onComplete(response: VKResponse?) {
                val user = (response?.parsedModel as VKList<VKApiUser>)[0]
                asyncCallsHandler.userPhoto(Uri.parse(user.photo_200))
            }
        })
        return null
    }

    override fun logout() {
        VKSdk.logout()
    }

}