package ru.solovgeo.tochkatestapp.data.net

import okhttp3.FormBody
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory

class ApiModule private constructor() {
    companion object {
        fun getApiInterface(baseUrl: String): GithubApi {
            val interceptor = HttpLoggingInterceptor()
            interceptor.level = HttpLoggingInterceptor.Level.BODY
            val httpClientBuilder = OkHttpClient.Builder()

            FormBody.Builder().build()
            httpClientBuilder.addInterceptor(interceptor)

            val client = httpClientBuilder.build()
            val retrofit = retrofit2.Retrofit.Builder()
                    .baseUrl(baseUrl)
                    .client(client)
                    .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                    .addConverterFactory(GsonConverterFactory.create())
                    .build()

            val githubApi = retrofit.create(GithubApi::class.java)
            return githubApi
        }
    }
}