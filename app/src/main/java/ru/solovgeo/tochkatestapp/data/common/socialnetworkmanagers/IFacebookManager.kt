package ru.solovgeo.tochkatestapp.data.common.socialnetworkmanagers

import android.content.Intent
import com.facebook.FacebookCallback
import com.facebook.login.LoginResult

interface IFacebookManager {
    fun login(callback: FacebookCallback<LoginResult>)
    fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?)
    fun isUserLoggedIn(): Boolean
}