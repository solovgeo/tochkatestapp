package ru.solovgeo.tochkatestapp.data.repository

import io.reactivex.Observable
import ru.solovgeo.tochkatestapp.data.entity.GithubUser
import ru.solovgeo.tochkatestapp.data.repository.datasource.GithubDataStoreFactory
import ru.solovgeo.tochkatestapp.other.Const
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class GithubDataRepo @Inject constructor(private val mGithubDataStoreFactory: GithubDataStoreFactory) : IGithubDataRepo {

    override fun searchGithubUsers(text: String, page: Int): Observable<List<GithubUser>> {
        val githubDataStore = mGithubDataStoreFactory.create()
        return githubDataStore.searchGithubUsers(text, page, Const.PAGINATION_PAGE_SIZE).map {
            it.items
        }
    }
}