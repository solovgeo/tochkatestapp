package ru.solovgeo.tochkatestapp.data.common.socialnetworkdataprovider

import android.content.Context
import android.net.Uri
import com.facebook.Profile
import com.facebook.ProfileTracker
import com.facebook.login.LoginManager
import ru.solovgeo.tochkatestapp.R
import javax.inject.Inject


class FacebookDataProvider @Inject constructor(private val mContext: Context) : ISocialNetworkDataProvider {

    override fun userName(asyncCallsHandler: ISocialNetworkDataProvider.AsyncCallsHandler): String? {
        return getDataFromProfile({ getSyncNameFromProfile(it) },
                { handler, profile -> getAsyncNameFromProfile(handler, profile) }, asyncCallsHandler)
    }

    override fun userPhoto(asyncCallsHandler: ISocialNetworkDataProvider.AsyncCallsHandler): Uri? {
        return getDataFromProfile({ getSyncUriFromProfile(it) },
                { handler, profile -> getAsyncUriFromProfile(handler, profile) }, asyncCallsHandler)
    }

    private fun <T> getDataFromProfile(sync: (Profile) -> T,
                                       async: (ISocialNetworkDataProvider.AsyncCallsHandler, Profile?) -> Unit,
                                       handler: ISocialNetworkDataProvider.AsyncCallsHandler): T? {
        val currentProfile = Profile.getCurrentProfile()
        if (currentProfile != null) {
            return sync(currentProfile)
        } else {
            object : ProfileTracker() {
                override fun onCurrentProfileChanged(oldProfile: Profile?, currentProfile: Profile?) {
                    if (currentProfile != null) {
                        async(handler, currentProfile)
                    } else if (oldProfile != null) {
                        async(handler, oldProfile)
                    }
                }
            }
            return null
        }
    }

    private fun getAsyncUriFromProfile(handler: ISocialNetworkDataProvider.AsyncCallsHandler, profile: Profile?) {
        handler.userPhoto(getSyncUriFromProfile(profile))
    }

    private fun getSyncUriFromProfile(profile: Profile?): Uri? {
        return profile?.getProfilePictureUri(
                mContext.resources.getDimension(R.dimen.circle_image_size).toInt(),
                mContext.resources.getDimension(R.dimen.circle_image_size).toInt())
    }

    private fun getAsyncNameFromProfile(handler: ISocialNetworkDataProvider.AsyncCallsHandler, profile: Profile?) {
        handler.userName(getSyncNameFromProfile(profile))
    }

    private fun getSyncNameFromProfile(profile: Profile?): String? {
        return profile?.firstName + " " + profile?.lastName
    }

    override fun logout() = LoginManager.getInstance().logOut()
}