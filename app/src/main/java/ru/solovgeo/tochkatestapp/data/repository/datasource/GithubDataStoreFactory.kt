package ru.solovgeo.tochkatestapp.data.repository.datasource

import ru.solovgeo.tochkatestapp.data.net.GithubApi
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class GithubDataStoreFactory @Inject constructor(private val mGithubApi: GithubApi) {

    fun create(): GithubDataStore {
        /* if(CacheIsValid){
        * return DiskGithubDataStore()
        * }
        * */
        return NetworkGithubDataStore(mGithubApi)
    }
}