package ru.solovgeo.tochkatestapp.data.entity


data class GithubUserList(
        val total_count: Int,
        val incomplete_results: Boolean,
        val items: List<GithubUser>
)

data class GithubUser(
        val login: String, //mojombo
        val id: Int, //1
        val avatar_url: String, //https://secure.gravatar.com/avatar/25c7c18223fb42a4c6ae1c8db6f50f9b?d=https://a248.e.akamai.net/assets.github.com%2Fimages%2Fgravatars%2Fgravatar-user-420.png
        val gravatar_id: String,
        val url: String, //https://api.github.com/users/mojombo
        val html_url: String, //https://github.com/mojombo
        val followers_url: String, //https://api.github.com/users/mojombo/followers
        val subscriptions_url: String, //https://api.github.com/users/mojombo/subscriptions
        val organizations_url: String, //https://api.github.com/users/mojombo/orgs
        val repos_url: String, //https://api.github.com/users/mojombo/repos
        val received_events_url: String, //https://api.github.com/users/mojombo/received_events
        val type: String, //User
        val score: Double //105.47857
)