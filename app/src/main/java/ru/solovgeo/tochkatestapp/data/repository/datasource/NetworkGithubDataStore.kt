package ru.solovgeo.tochkatestapp.data.repository.datasource

import io.reactivex.Observable
import ru.solovgeo.tochkatestapp.data.entity.GithubUserList
import ru.solovgeo.tochkatestapp.data.net.GithubApi

class NetworkGithubDataStore(private val mGithubApi: GithubApi) : GithubDataStore {
    override fun searchGithubUsers(text: String, page: Int, pageSize: Int): Observable<GithubUserList> {
        return mGithubApi.getUsers(text, page, pageSize)
    }
}