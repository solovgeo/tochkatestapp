package ru.solovgeo.tochkatestapp.data.common

import android.content.Context
import android.net.Uri
import android.widget.ImageView
import com.squareup.picasso.Picasso
import com.squareup.picasso.RequestCreator
import javax.inject.Inject
import javax.inject.Singleton


@Singleton
class ImageLoader @Inject constructor(private val mContext: Context) : IImageLoader {

    override fun getImageRequestCreator(uri: Uri, imageView: ImageView) {
        return Picasso.with(mContext).load(uri).into(imageView)
    }

    override fun getImageRequestCreator(uri: Uri): RequestCreator {
        return Picasso.with(mContext).load(uri)
    }
}