package ru.solovgeo.tochkatestapp.data.net

import io.reactivex.Observable
import retrofit2.http.GET
import retrofit2.http.Query
import ru.solovgeo.tochkatestapp.data.entity.GithubUserList

interface GithubApi {

    @GET("/search/users")
    fun getUsers(@Query("q") text: String,
                 @Query("page") page: Int,
                 @Query("page_size") pagesize: Int): Observable<GithubUserList>
}