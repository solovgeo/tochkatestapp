package ru.solovgeo.tochkatestapp.data.common.socialnetworkmanagers

import android.content.Intent

interface IGoogleManager {
    fun login()
    fun onActivityResult(data: Intent?, googleCallback: GoogleManager.GoogleCallback)
    fun isUserLoggedIn(): Boolean
}